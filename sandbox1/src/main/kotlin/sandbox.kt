package org.example.sandbox1

/**
 * Integer addition
 * @param a1 First argument
 * @param a2 Second argument
 * @return addition
 */
fun add(a1: Int, a2: Int) = a1 + a2
